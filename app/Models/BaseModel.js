'use strict'

/** @type {typeof import('App/Models/BaseModel')} */
const Model = use('Model')

class BaseModel extends Model {

  static defaultIncludes () {
    return [
    ]
  }

  static availableIncludes () {
    return [
      ...this.defaultIncludes(),
    ]
  }
}

module.exports = BaseModel
