'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Locale extends Model {
  static get computed () {
    return ['locale']
  }

  /* Relations */
  // TODO; Figure out if we really want all the relations
  // on the locale-model
  pageTranslations () {
    return this.hasMany('App/Models/PageTranslation')
  }

  getLocale ({ language_code, country_code }) {
    return `${language_code}_${country_code}`
  }
}

module.exports = Locale
