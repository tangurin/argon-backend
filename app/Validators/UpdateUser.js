'use strict'

class SaveUser {
  get rules () {
    const userId = this.ctx.params.id

    return {
      username: `unique:users,username,id,${userId}`,
      email: `email|unique:users,email,id,${userId}`,
    }
  }

  get messages () {
    // TODO: replace errors with language strings
    return {
      'email.required': 'Email is required',
    }
  }

  get sanitizationRules () {
    return {
      email: 'normalize_email',
    }
  }
}

module.exports = SaveUser
