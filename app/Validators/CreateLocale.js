'use strict'

class CreateLocale {
  get rules () {
    return {
      country_code: 'required',
      language_code: 'required',
      name: 'required|unique:locales',
    }
  }

  get messages () {
    // TODO: replace errors with language strings
    return {
      'country_code.required': 'Country code is required',
      'language_code.required': 'Language code is required',
      'name.required': 'Name is required',
      'name.unique': 'Name is already used',
    }
  }
}

module.exports = CreateLocale
