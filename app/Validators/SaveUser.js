'use strict'

class SaveUser {
  get rules () {
    return {
      username: 'required|unique:users',
      email: 'required|email|unique:users',
      password: 'required',
    }
  }

  get messages () {
    // TODO: replace errors with language strings
    return {
      'username.required': 'Username is required',
      'email.required': 'Email is required',
      'email.email': 'Email is not valid',
      'email.unique': 'Email is already in use',
      'password.required': 'Password is required',
    }
  }

  get sanitizationRules () {
    return {
      email: 'normalize_email',
    }
  }
}

module.exports = SaveUser
