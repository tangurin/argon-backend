'use strict'

class UpdateLocale {
  get rules () {
    const localeId = this.ctx.params.id

    return {
      name: `unique:locales,name,id,${localeId}`,
    }
  }

  get messages () {
    // TODO: replace errors with language strings
    return {
      'name.unique': 'Name is already taken',
    }
  }
}

module.exports = UpdateLocale
