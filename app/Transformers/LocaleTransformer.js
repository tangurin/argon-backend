'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * LocaleTransformer class
 *
 * @class LocaleTransformer
 * @constructor
 */
class LocaleTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform (locale) {
    return {
      id: locale.id,
      active: locale.active,
      default: locale.default,
      country_code: locale.country_code,
      language_code: locale.language_code,
      name: locale.name,
    }
  }
}

module.exports = LocaleTransformer
