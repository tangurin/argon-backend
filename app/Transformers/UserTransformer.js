'use strict'

const BumblebeeTransformer = use('Bumblebee/Transformer')

/**
 * UserTransformer class
 *
 * @class UserTransformer
 * @constructor
 */
class UserTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data by default.
   */
  transform (user) {
    return {
      id: user.id,
      username: user.username,
      email: user.email,
    }
  }

  // To use this, run:
  // transform.collection(users, 'UserTransformer.backoffice')
  // OR
  // transform.usingVariant('backoffice').collection(users, this.transformer)
  transformBackoffice (user) {
    return {
      ...this.transform(user),
      created_at: user.created_at,
      updated_at: user.updated_at,
    }
  }
}

module.exports = UserTransformer
