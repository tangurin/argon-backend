'use strict'

const Locale = use('App/Models/Locale')
const Redis = use('Redis')
const Request = use('Adonis/Src/Request')


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class LocaleDetector {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request }, next) {
    // call next to advance the request
    const cachedLocales = await Redis.get('locales')
    let availableLocales = []


    if (cachedLocales) {
      availableLocales = JSON.parse(cachedLocales)
    } else {
      const jsonLocales = JSON.stringify(await Locale.all())
      await Redis.set('locales', jsonLocales)
      availableLocales = JSON.parse(jsonLocales)
    }

    // Find the active locale
    let selectedLocale = availableLocales.find(function (locale) {
      return locale.locale == request.header('Accept-Locale')
    })

    // If the locale is not found, use the default
    if (selectedLocale === undefined) {
      selectedLocale = availableLocales.find(function (locale) {
        return locale.default == true
      })
    }

    // Set the locale id on the request to be accessible
    // everywhere the request is available
    Request.macro('localeId', function () {
      return selectedLocale.id
    })

    await next()
  }
}

module.exports = LocaleDetector
