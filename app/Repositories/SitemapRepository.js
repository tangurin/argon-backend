'use strict'

const Page = use('App/Models/Page')

const { SitemapStream, streamToPromise } = require('sitemap')
const { createGzip } = require('zlib')
const { ioc } = require('@adonisjs/fold')

class SitemapRepository{
  async pages(localeId='1') {
    return await Page
      .query()
      .with('pageTranslations', (builder) => {
        builder.where('locale_id', localeId)
      })
      .where('active', 1)
      .fetch()
  }
}

ioc.singleton('SitemapRepository', function (app) {
  return new SitemapRepository()
})

module.exports = ioc.use('SitemapRepository')
