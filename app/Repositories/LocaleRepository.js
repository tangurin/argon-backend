'use strict'

const { ioc } = require('@adonisjs/fold')
const BaseRepository = use('App/Repositories/BaseRepository')
const LocaleTransformer = use('App/Transformers/LocaleTransformer')

class LocaleRepository extends BaseRepository{
  constructor(model){
    super(model, LocaleTransformer)
    this.model = model
  }

}

ioc.singleton('LocaleRepository', function (app) {
  const Model = app.use('App/Models/Locale')
  return new LocaleRepository(Model)
})

module.exports = ioc.use('LocaleRepository')
