'use strict'

/*
 * All commonly used functions are placed here, mostly CRUD operations
 */
const Env = use('Env')


class BaseRepository{

  constructor(model, transformer){
    this.model = model
    this.transformer = transformer
    this.noRecordFound = 'No record found'
  }

  static addFilters(modelQuery, filters){
    Object.entries(filters).forEach(([filter,value])=>{
      modelQuery.where(filter, value)
    })
  }

  static addSorting(modelQuery, sortOptions){
    if (typeof sortOptions !== 'undefined') {
      sortOptions.split(",").forEach(sortOption => {
        let sort = 'asc'
        if (sortOption[0] == '-') {
          sort = 'desc'
          sortOption = sortOption.substr(1)
        }

        modelQuery.orderBy(sortOption, sort)
      })
    }
  }

  static addIncludes(modelQuery, model, requestInclude){
    let includes = []

    // Eagerload all the default includes for the model
    model.defaultIncludes().forEach(defaultInclude => {
      modelQuery.with(defaultInclude)
    })

    // Eagerload the request-specified includes for the model
    if (typeof requestInclude !== 'undefined') {
      requestInclude.split(",").forEach(include => {
        if (model.defaultIncludes().includes(include)) {
          return
        }

        if (model.availableIncludes().includes(include)) {
          modelQuery.with(include)
          includes.push(include)
        }
      })
    }

    return includes
  }

  //Get all records
  async index(params, request, response, transform){
    // should support filter, sort, pagination
    const requestParameters = request.get()
    const filters = request.except(['include', 'sort', 'page', 'limit'])
    // TODO: (Minor) the default values for page and limit should be put somewhere else. Perhaps be possible to be overridden by env file?
    const page = parseInt(requestParameters.page) || 1
    const limit = parseInt(requestParameters.limit) || 30

    let modelQuery = this.model.query()

    // Sort
    this.constructor.addSorting(modelQuery, requestParameters.sort)

    const includes = this.constructor.addIncludes(modelQuery, this.model, requestParameters.include)

    this.constructor.addFilters(modelQuery, filters)

    // TODO: fetch should depend on logged in used
    let models = await modelQuery.paginate(page, limit)

    return {
      status: 200,
      data: await transform.include(includes.toString()).collection(models, this.transformer),
      // TODO: Refactor links. Looks a bit messy, but gets the job done. :)
      links: {
        first: Env.get('APP_URL') + request.originalUrl().replace(/(page=)\d/, 'page=1'),
        last: Env.get('APP_URL') + request.originalUrl().replace(/(page=)\d/, 'page=' + models.pages.lastPage),
        prev: (page - 1) > 0 == true ? Env.get('APP_URL') + request.originalUrl().replace(/(page=)\d/, 'page=' + (page - 1)) : null,
        next: (page + 1) <= models.pages.lastPage == true ? Env.get('APP_URL') + request.originalUrl().replace(/(page=)\d/, 'page=' + (page + 1)) : null,
        self: Env.get('APP_URL') + request.originalUrl(),
      }
    }
  }


  //Save a record
  async store(request, response, transform){
    // should support includes (in the future)
    let input = request.except(['password_confirmation'])
    let modelObj = new this.model()

    /*
     * check if the input is not empty -> No need to check here, validator on route will take care of this
     */

    //assigning input data in db fields
    Object.entries(input).forEach(([key,value])=>{
      modelObj[key] = value
    })

    await modelObj.save()

    return {
      status: 201,
      msg: this.model.name + ' created successfully',
      data: await transform.item(modelObj, this.transformer)
    }
  }


  //Show single record
  async show(params, request, response, transform){
    // should support includes
    const requestParameters = request.get()
    let modelQuery = this.model.query()

    const includes = this.constructor.addIncludes(modelQuery, this.model, requestParameters.include)

    const modelObj = await modelQuery.where('id', params.id).first()

    if(!modelObj){
      return {
        status: 404,
        msg: this.noRecordFound
      }
    }

    return {
      status: 200,
      data: await transform.include(includes.toString()).item(modelObj, this.transformer),
    }
  }


  //Update a record
  async update(params, request, response, transform){
    // should support includes (in the future)
    const input = request.all()
    const modelObj = await this.model.find(params.id)

    //check if the row related to this id exists
    if(!modelObj){
      return {
        status: 404,
        msg: this.noRecordFound
      }
    }

    /*
    *check if the input is not empty -> No need to check here, validator on route will take care of it
    */

    //assigning input data in db fields
    Object.entries(input).forEach(([key,value])=>{
      modelObj[key] = value
    })

    await modelObj.save()


    return {
      status: 200,
      msg: this.model.name + ' has been updated',
      data: await transform.item(modelObj, this.transformer)
    }
  }


  async destroy(params, response, transform){
    const modelObj = await this.model.find(params.id)
    if(!modelObj){
      return {
        status: 404,
        msg: this.noRecordFound
      }
    }
    await modelObj.delete()

    return {
      status: 200,
      msg: this.model.name + ' deleted',
      data: await transform.item(modelObj, this.transformer)
    }
  }
}

module.exports = BaseRepository
