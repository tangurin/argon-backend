'use strict'

const { ioc } = require('@adonisjs/fold')
const BaseRepository = use('App/Repositories/BaseRepository')
const UserTransformer = use('App/Transformers/UserTransformer')

class UserRepository extends BaseRepository{
  constructor(model){
    super(model, UserTransformer)
    this.model = model
  }

}

ioc.singleton('UserRepository', function (app) {
  const Model = app.use('App/Models/User')
  return new UserRepository(Model)
})

module.exports = ioc.use('UserRepository')
