'use strict'

const UserRepo = use('UserRepository')
const User = use('App/Models/User')

class AuthController {

  async register({request, auth, response, transform}) {
    let storeResponse = await UserRepo.store(request, response, transform)
    const user = await User.find(storeResponse.data.id)

    let accessToken = await auth.generate(user)
    return response.json({'user': user, 'access_token': accessToken})
  }

  async login({request, auth, response}) {
    const email = request.input('email')
    const password = request.input('password');
    try {
      if (await auth.attempt(email, password)) {
        let user = await User.findBy('email', email)
        let accessToken = await auth.generate(user)
        return response.json({'user':user, 'access_token': accessToken})
      }

    }
    catch (e) {
      return response.json({message: 'You first need to register!'})
    }
  }

}

module.exports = AuthController
