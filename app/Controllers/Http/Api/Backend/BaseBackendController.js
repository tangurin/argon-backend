'user-strict'


/*
 * All commonly used functions are placed here, mostly CRUD calls
 */


class BaseBackendController{

  constructor(repo){
    this.repo = repo
  }

  async index({response, params, transform}){
    return this.returnReponse(response, await this.repo.index(response, params, transform))
  }

  async store({request, response, transform}){
    return this.returnReponse(response, await this.repo.store(request, response, transform))
  }

  async show({params, response, transform}){
    return this.returnReponse(response, await this.repo.show(params, response, transform))
  }

  async update({params, request, response, transform}){
    return this.returnReponse(response, await this.repo.update(params, request, response, transform))
  }

  async destroy({params, response, transform}){
    return this.returnReponse(response, await this.repo.destroy(params, response, transform))
  }

  async returnReponse(response, data) {
    return response.status(data.status).json({
      msg: data.msg,
      data: data.data,
    })
  }

}

module.exports = BaseBackendController
