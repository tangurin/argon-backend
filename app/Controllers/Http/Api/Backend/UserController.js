'use strict'

const UserRepo = use('UserRepository')
const BaseBackendController = use('BaseBackendController')
class UserController extends BaseBackendController{

  constructor(){
    super(UserRepo)
    this.repo=UserRepo
  }

}

module.exports = UserController
