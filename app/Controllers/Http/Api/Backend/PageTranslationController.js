'use strict'

const PageTranslationRepo = use('PageTranslationRepository')
const BaseBackendController = use('BaseBackendController')
class PageTranslationController extends BaseBackendController{

  constructor(){
    super(PageTranslationRepo)
    this.repo=PageTranslationRepo
  }

}

module.exports = PageTranslationController
