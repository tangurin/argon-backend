'use strict'

const LocaleRepo = use('LocaleRepository')
const BaseBackendController = use('BaseBackendController')
class LocaleController extends BaseBackendController{

  constructor(){
    super(LocaleRepo)
    this.repo=LocaleRepo
  }

}

module.exports = LocaleController
