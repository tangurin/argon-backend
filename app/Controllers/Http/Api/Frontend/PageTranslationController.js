'use strict'

const PageTranslationRepo = use('PageTranslationRepository')
const BaseFrontendController = use('BaseFrontendController')
class PageTranslationController extends BaseFrontendController{

  constructor(){
    super(PageTranslationRepo)
    this.repo=PageTranslationRepo
  }

}

module.exports = PageTranslationController
