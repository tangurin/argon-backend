'use strict'

const Page = use('App/Models/Page')

const Env = use('Env')
const { SitemapStream, streamToPromise } = require('sitemap')
const SitemapRepo = use('SitemapRepository')

class SitemapController {

  async generate(response) {
    response.response.header('Content-type', 'application/xml')

    const sitemap = new SitemapStream({ hostname: Env.get('APP_URL') });

    let pages = await SitemapRepo.pages()
    for(let i in pages.rows) {
      const page = pages.rows[i].toJSON()
      // TODO: replace page.pageTranslations[0] with something more failsafe
      const pageTranslation = page.pageTranslations[0];
      sitemap.write({
        url: '/pages/' + pageTranslation.slug,
        changefreq: pageTranslation.changefreq,
        priority: pageTranslation.priority
      })
    }

    // smStream.end()
    sitemap.end()

    return await streamToPromise(sitemap);
  }

  async pages() {

    // const page = await Page.find(1)
    // return await page
    //   .pageTranslations()
    //   .fetch()

    return await Page
      .query()
      .with('pageTranslations', (builder) => {
        builder.where('locale_id', 1)
      })
      .where('active', 1)
      .fetch()
  }
}

module.exports = SitemapController
