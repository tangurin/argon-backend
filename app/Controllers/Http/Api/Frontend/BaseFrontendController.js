'user-strict'


/*
 * All commonly used functions are placed here, mostly CRUD calls
 */


class BaseFrontendController{

  constructor(repo){
    this.repo = repo
  }

  async index({params, request, response,  transform}){
    return this.returnReponse(response, await this.repo.index(params, request, response, transform))
  }

  async store({request, response, transform}){
    return this.returnReponse(response, await this.repo.store(request, response, transform))
  }

  async show({params, request, response, transform}){
    return this.returnReponse(response, await this.repo.show(params, request, response, transform))
  }

  async update({params, request, response, transform}){
    return this.returnReponse(response, await this.repo.update(params, request, response, transform))
  }

  async destroy({params, response, transform}){
    return this.returnReponse(response, await this.repo.destroy(params, response, transform))
  }

  async returnReponse(response, data) {
    return response.status(data.status).json({
      msg: data.msg,
      data: data.data,
      links: data.links,
    })
  }

}

module.exports = BaseFrontendController
