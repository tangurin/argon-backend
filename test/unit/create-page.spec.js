'use strict'

const Factory = use('Factory')
const { test, trait } = use('Test/Suite')('Create Page')

trait('Test/ApiClient')
trait('Auth/Client')


test('Create a new page page', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()

  const inputData = {
    active: 1,
    name: 'startpage',
    type: 'page',
    parent_id: 0,
  }

  const response = await client
        .post('/pages')
        .loginVia(user, 'jwt')
        .send(inputData)
        .end()

  response.assertStatus(201)
  // TODO: create pageTranslations as well, vat the data from that object is also received here
  response.assertJSONSubset({
    msg: 'Page created successfully',
    data: {
      active: inputData.active,
      name: inputData.name,
      type: inputData.type,
      parent_id: inputData.parent_id,
    }
  })
})
