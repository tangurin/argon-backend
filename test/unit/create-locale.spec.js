'use strict'

const Factory = use('Factory')
const { test, trait } = use('Test/Suite')('Create Locale')

trait('Test/ApiClient')
trait('Auth/Client')

test('Create a new locale', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()

  const inputData = {
    active: '1',
    country_code: 'SE',
    language_code: 'sv',
    name: 'swedish',
  }

  const response = await client
        .post('/locales')
        .loginVia(user, 'jwt')
        .send(inputData)
        .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    msg: 'Locale created successfully',
    data: {
      active: inputData.active,
      country_code: inputData.country_code,
      language_code: inputData.language_code,
      name: inputData.name,
    }
  })
})
