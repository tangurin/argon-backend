'use strict'

const Factory = use('Factory')
const { test, trait } = use('Test/Suite')('Create Page')

trait('Test/ApiClient')
trait('Auth/Client')


test('Create a new page page with translation data', async ({ assert, client }) => {
  const user = await Factory.model('App/Models/User').create()
  const language = await Factory.model('App/Models/Locale').create()

  const page = await Factory.model('App/Models/Page').create()

  const inputData = {
    page_id: page.id,
    locale_id: language.id,
    name: 'english titel',
    slug: 'test-page-en',
    content: '<div>content of the page</div>',
    description: 'description data',
    title: 'page title',
    keywords: 'keyword1, keyword2',
    changefreq: 'daily',
    priority: '0.5',
  }

  const response = await client
        .post('/pages/' + page.id + '/translations')
        .loginVia(user, 'jwt')
        .send(inputData)
        .end()

  response.assertStatus(201)
  response.assertJSONSubset({
    msg: 'PageTranslation created successfully',
    data: {
      page_id: page.id,
      locale_id: language.id,
      name: inputData.name,
      slug: inputData.slug,
      content: inputData.content,
      description: inputData.description,
      title: inputData.title,
      keywords: inputData.keywords,
      changefreq: inputData.changefreq,
      priority: inputData.priority,
    }
  })
})
