'use strict'

const Factory = use('Factory')
const { test, trait } = use('Test/Suite')('Create User')

trait('Test/ApiClient')
trait('Auth/Client')

test('Can authenticate a user', async ({ assert, client }) => {

  const user = await Factory.model('App/Models/User').create()

  const response = await client
        .get('/users')
        .loginVia(user, 'jwt')
        .end()

  response.assertStatus(200)
})
