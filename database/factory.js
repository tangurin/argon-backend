'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/User', (faker, i, data) => {
  return {
    username: faker.username(),
    email: faker.email(),
    password: 'password',
    ...data,
  }
})

Factory.blueprint('App/Models/Page', (faker, i, data) => {
  return {
    active: 1,
    name: faker.word(),
    type: 'page',
    parent_id: 0,
    ...data,
  }
})

Factory.blueprint('App/Models/Locale', (faker, i, data) => {
  return {
    active: '1',
    country_code: 'US',
    language_code: 'en',
    name: 'english',
    ...data,
  }
})
