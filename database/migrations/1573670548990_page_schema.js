'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PageSchema extends Schema {
  up () {
    this.create('pages', (table) => {
      table.increments()
      table.boolean('active')
      table.string('name', 254).notNullable().unique()
      table.string('type', 254).notNullable()
      table.integer('parent_id').unsigned().defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('pages')
  }
}

module.exports = PageSchema
