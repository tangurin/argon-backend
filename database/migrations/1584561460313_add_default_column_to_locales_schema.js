'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddDefaultColumnToLocalesSchema extends Schema {
  up () {
    this.table('locales', (table) => {
      table.boolean('default').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('locales', (table) => {
      table.dropColumn('default')
    })
  }
}

module.exports = AddDefaultColumnToLocalesSchema
