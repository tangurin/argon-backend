'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PageTranslationSchema extends Schema {
  up () {
    this.create('page_translations', (table) => {
      table.increments()
      table.integer('page_id').unsigned().references('id').inTable('pages')
      table.integer('locale_id').unsigned().references('id').inTable('locales')
      table.string('name', 254).notNullable()
      table.string('slug', 254).notNullable()
      table.text('content')
      table.text('description')
      table.string('title', 254)
      table.string('keywords', 254)
      table.string('changefreq', 254)
      table.string('priority', 254)
      table.timestamps()
    })
  }

  down () {
    this.drop('page_translations')
  }
}

module.exports = PageTranslationSchema
