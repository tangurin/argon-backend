'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocaleSchema extends Schema {
  up () {
    this.create('locales', (table) => {
      table.increments()
      table.boolean('active')
      table.string('country_code', 254).notNullable() // ISO 3166-1
      table.string('language_code', 254).notNullable() // ISO 639-1
      table.string('name', 254).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('locales')
  }
}

module.exports = LocaleSchema
