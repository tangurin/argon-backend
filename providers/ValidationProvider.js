'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class ValidationProvider extends ServiceProvider {

  /**
   * Adds the exists rule to the validator
   *
   * @method _addExistsRule
   *
   * @private
   */
  _addExistsRule (data, field, message, args, get) {
    return new Promise((resolve, reject) => {
      const Database = use('Database')

      const value = get(data, field)

      /**
       * if value is not defined, then skip the validation
       * since required rule should be added for required
       * fields
       */
      if (!value) {
        return resolve('validation skipped')
      }

      const [table, fieldName, ignoreKey, ignoreValue] = args

      const query = Database.table(table).where(fieldName || field, value).first()

      /**
       * If a ignore key and value is defined, then add a whereNot clause
       */
      if (ignoreKey && ignoreValue) {
        query.whereNot(ignoreKey, ignoreValue)
      }

      query
        .then((rows) => {
          /**
           * Exists validation fails when a row has been found
           */

          if (!rows) {
            return reject(message)
          }

          resolve('validation passed')
        })
        .catch(reject)
    })

  }


  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register () {
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot () {
    const { extend } = use('Adonis/Addons/Validator')

    /**
     * Extend by adding custom rules
     */
    extend('exists', this._addExistsRule.bind(this), '{{field}} does not exist in {{table}} table')
  }
}

module.exports = ValidationProvider
