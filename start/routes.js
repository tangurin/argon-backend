'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// JWT auth routes
Route.post('/auth/register', 'AuthController.register')
Route.post('/auth/login', 'AuthController.login')

Route.get('/sitemap.xml', 'Api/Frontend/SitemapController.generate')

// Routes that require user authentication
Route.group(() => {
  Route
    .resource('users', 'Api/Backend/UserController')
    .apiOnly()
    .except([
      'store' // Disabled since the auth controller handles creation.
    ])
    .validator(new Map([
      [['users.store'], ['SaveUser']],
      [['users.update'], ['UpdateUser']]
    ]))

  Route
    .resource('locales', 'Api/Backend/LocaleController')
    .apiOnly()
    .validator(new Map([
      [['locales.store'], ['CreateLocale']],
      [['locales.update'], ['UpdateLocale']]
    ]))

}).prefix('backoffice').middleware(['auth'])
