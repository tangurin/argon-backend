'use strict'

var path=require('path');

/*
|--------------------------------------------------------------------------
| Providers
|--------------------------------------------------------------------------
|
| Providers are building blocks for your Adonis app. Anytime you install
| a new Adonis specific package, chances are you will register the
| provider here.
|
*/
const providers = [
  // Adonis providers
  '@adonisjs/framework/providers/AppProvider',
  '@adonisjs/auth/providers/AuthProvider',
  '@adonisjs/bodyparser/providers/BodyParserProvider',
  '@adonisjs/cors/providers/CorsProvider',
  '@adonisjs/lucid/providers/LucidProvider',
  '@adonisjs/validator/providers/ValidatorProvider',
  '@adonisjs/redis/providers/RedisProvider',


  // Custom providers
  'adonis-bumblebee/providers/BumblebeeProvider',
  path.join(__dirname, '..', 'providers', 'ValidationProvider'),

  // Argon modules
  '@argon/cms/providers/CmsProvider',
]

/*
|--------------------------------------------------------------------------
| Ace Providers
|--------------------------------------------------------------------------
|
| Ace providers are required only when running ace commands. For example
| Providers for migrations, tests etc.
|
*/
const aceProviders = [
  '@adonisjs/lucid/providers/MigrationsProvider',
  'adonis-bumblebee/providers/CommandsProvider',
  '@adonisjs/vow/providers/VowProvider',
]

/*
|--------------------------------------------------------------------------
| Aliases
|--------------------------------------------------------------------------
|
| Aliases are short unique names for IoC container bindings. You are free
| to create your own aliases.
|
| For example:
|   { Route: 'Adonis/Src/Route' }
|
*/
const aliases = {

  //Repositories
  BaseRepository:'App/Repositories/BaseRepository',
  UserRepository:'App/Repositories/UserRepository',
  LocaleRepository:'App/Repositories/LocaleRepository',
  SitemapRepository:'App/Repositories/SitemapRepository',

  //Controllers
  BaseBackendController: 'App/Controllers/Http/Api/Backend/BaseBackendController',
  BaseFrontendController: 'App/Controllers/Http/Api/Frontend/BaseFrontendController',

}

/*
|--------------------------------------------------------------------------
| Commands
|--------------------------------------------------------------------------
|
| Here you store ace commands for your package
|
*/
const commands = []

module.exports = { providers, aceProviders, aliases, commands }
